
from django.contrib import admin
from django.urls import path




from products.views import product_create_view,product_detail_view

urlpatterns = [
    path('create/',product_create_view,name='create'),
    path('detail/',product_detail_view.as_view(),name='detail'),
  

]
