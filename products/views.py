from django.shortcuts import render
from .models import Product
from .forms import ProductForm
from django.views.generic import ListView

def product_create_view(request):
   form = ProductForm(request.POST or None)
   if form.is_valid():
       form.save()
       form=ProductForm()

   context ={
       'form':form
   }
   return render(request,'product/product_create.html',context)





class product_detail_view(ListView):
    template_name = "product/detail.html"
    queryset = Product.objects.all().order_by("-id")
    context_object_name = "allproduct"



