from django.db import models


class Product(models.Model):
    title           = models.CharField(max_length=150)
    description     = models.TextField(blank=True)
    price           = models.DecimalField(decimal_places=2,max_digits=1000)
    images          = models.ImageField(upload_to='images')